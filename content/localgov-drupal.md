## LocalGov Drupal

Since 2019 I have been lucky enough to be working on LocalGov Drupal, a Drupal distribution built and supported by a collaboration of councils and suppliers.

### Services

Through Open Code Ltd I can provide a range of services that might help smooth your journey with LocalGov Drupal.

 - [LocalGov Drupal Consultancy]({{< ref "/localgov-drupal-consultancy" >}} "LocalGov Drupal Consultancy")
 - LocalGov Drupal Training
 - LocalGov Drupal Delivery Management
 - LocalGov Drupal Drupal Development
 - General Drupal Training

### History

In late 2019 we spotted a tender from Croydon Council to run a discovery phase to explore how councils could collaborate on an open source Drupal