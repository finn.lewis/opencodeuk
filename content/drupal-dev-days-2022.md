


Day 1:

Session 1: https://drupalcamp.be/en/drupal-dev-days-2022/session/setting-solid-github-cicd

A great introduction to running CI on Github.

https://github.com/robiningelbrecht/continuous-integration-example

Key take aways:

1. Add labels to the repo PHP Stan level, licence, tests passing etc.
2. Use https://app.codecov.io to visualise your testing.
3. Check https://github.com/robiningelbrecht/continuous-integration-example for examples.

Session 2.

Progressive decoupling: A how-to with an example from the World Health Organisation

https://drupalcamp.be/en/drupal-dev-days-2022/session/progressive-decoupling-how-example-world-health-organisation


https://www.drupal.org/project/anu_lms
https://www.drupal.org/project/rest_entity_recursive
https://developers.google.com/web/tools/workbox

https://systemseed.com/DDD2022

Key take aways:

1. The https://www.drupal.org/project/anu_lms is a neat place to start an LMS.
2. Preview in PWA is much easier than a fully decoupled.


## Day 2

The Fediverse!
https://docs.joinmastodon.org/user/run-your-own/

https://fediverse.party/
https://joinpeertube.org/

