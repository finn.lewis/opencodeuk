# Open Code Ltd.

Hello! I'm Finn, founder of Open Code Ltd an open source company providing consultancy, software development, agile delivery and technical leadership, and training. I provide most services myself, but also bring on other experienced and trusted professionals either for discrete assignments or to put together an multidisciplinary agile delivery team.

## Drupal

I'm primarily a Drupal developer and Drupal evangelist, based in Oxford UK. I've been working with Drupal since 2006, mostly at Agile Collective since 2011. I love developing with Drupal, writing complex migrations, creating custom modules, extending contributed module and collaborating with the community. Contribution is important to me and very much at the heart of the ethos of the company.

## Agile delivery management

As a certified scrum master, I've facilitated agile scrum teams on projects of various sizes, with budgets from tens of thousands to hundreds of thousands of pounds and I have a strong commitment to the agile methodology. The focus on user needs and evidence based design remains as valid and valuable today as when the agile manifesto was written.

## Consultancy

Sometimes you need someone with experience in Drupal, agile delivery or web development in general to listen and offer advise in a consultancy role. I am more than happy to engage as a part time consultant on open source, non-profit and public sector projects.

## Training

I love Drupal and I love helping people improve their Drupal skills, so training is a natural fit for me. I've delivered Drupal training at a number og councils over the years. Read more about the [Drupal training]({{< ref "/drupal-training" >}}) I can offer.

## LocalGov Drupal

Over the last few years, I've mainly been working as tech lead and delivery manager for the LocalGov Drupal distribution and the LocalGov Drupal Microsites distribution.
I am more than happy to provide specific services that build on my experience with LocalGov Drupal and open source and collaboration with councils.
Read more about various [LocalGov Drupal services]({{< ref "/localgov-drupal" >}}) that might be of interest.