## LocalGov Drupal Consultancy

I have been fortunate enough to be the tech lead and delivery manger for a number of key phases of the LocalGov Drupal project.

Building on my experience working with Drupal since 2006 and with Drupal at a number of UK councils since 2015, we put together an amazing team of Drupal experts to deliver the discovery, alpha, beta and microsites phases of the project. Throughout 2021 and 2022 I have overseen the development and deployment of the LocalGov Drupal distribution abd the LocalGov Drupal Microsites distribution, so I have an extensive understanding of the code and the community.

At the start of 2023, I continue to work as the tech lead for the open source community and am a co-director of the Open Digital Cooperative set up to support the ongoing collaboration.

If you think my knowledge could be helpful for you project, please [get in touch]({{< ref "/contact" >}}) and we can explore working together.
