# Open Code Ltd.

Hello! I'm Finn, founder of Open Code Ltd an open source company providing consultancy, software development, agile delivery and technical leadership, and training. I provide most services myself, but also bring on other experienced and trusted professionals either for discrete assignments or to put together an multidisciplinary agile delivery team.

## Drupal

I'm primarily a Drupal developer and Drupal evangelist, based in Oxford UK. I've been working with Drupal since 2006, mostly at Agile Collective since 2011. I love developing with Drupal, writing complex migrations, creating custom modules, extending contributed module and collaborating with the community. Contribution is important to me and very much at the heart of the ethos of the company.

## Agile delivery management

As a certified scrum master, I've facilitated agile scrum teams on projects of various sizes, with budgets from tens of thousands to hundreds of thousands of pounds and I have a strong commitment to the agile methodology. The focus on user needs and evidence based design remains as valid and valuable today as when the agile manifesto was written.

## Consultancy

Sometimes you need someone with experience in Drupal, agile delivery or web development in general to listen and offer advise in a consultancy role. I am more than happy to engage as a part time consultant on open source, non-profit and public sector projects.
