# Drupal training

I love Drupal and I love helping people improve their Drupal skills!

If you or your team need some bespoke Drupal training, consultancy or any sort of help with your Drupal skills, I'd be happy to jump on a call to discuss your needs.

I've been working with Drupal since 2006, mostly at (Agile Collective)[https://agile.coop] since 2011. I love developing with Drupal, writing complex migrations, creating custom modules, extending contributed module and collaborating with the community. Contribution is important to me and very much at the heart of the ethos of the company. I'd be happy to share my enthusiasm and experience with you and your team to help get you all to the next level.

## On-site Drupal training

I'm happy to travel to join individuals or teams in the office and understand your specific needs. Often working with Drupal reveals issues specific to your network, hosting infrastructure or local development environments.

## Remote Drupal training

After spending much of the last couple of years on Zoom calls most of us are super keen to get back to meeting in person. One to one training over Zoom (or Google Meet, Teams etc.) is actually very effective as it is easy to share screens to demonstrate approaches or to talk the learner through techniques.

